# Work Dispatcher
A simple `fzf` wrapper to help manage scripts to start projects. I often
find myself hesitant to restart my computer, I hate opening things back up. To
combat this I often write scripts to start everything up, but then I always
forget where I put them. This acts to streamline that.

![](media/screenshot_terminal_work-dispatcher.png)

## Usage
A collection of `.py` scripts should be located under `~/.local/bin`, then 
from the terminal run `workdispatch open` and a `fzf` menu will open
offering a selection of those scripts to run.

## Installation

It's probably easiest to just copy this into `~/.local/bin/` and manage it
with the rest of your DotFiles, this has the advantage that it can be
modified and adjusted:

* Just copy the file into the `~/.local/bin`  or anything in `$PATH`

  ```bash
  cargo install mdcat
  sudo apt install fzf
  mkdir "~/.local/work-dispatcher"
  curl 'https://gitlab.com/RyanGreenup/work-dispatcher/-/raw/main/workdispatch' \
      > ~/.local/bin
  ```

### Arch
There's also a package build:

```bash
cd "$(mktemp -d)"
curl 'https://gitlab.com/RyanGreenup/work-dispatcher/-/raw/main/AUR/PKGBUILD' -O
mkdir ~/.local/work-dispatcher/
makepkg -si
```